import React, { useState, useEffect } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { Container, Row, Col, Button } from 'react-bootstrap';
import './step1.css';

export default function Step1({
  chooseDate,
  chooseFacility,
  setCurrentStepNext,
}) {
  const [date, setDate] = useState(new Date());
  const [facility, setFacility] = useState();
  const [facilities, setFacilities] = useState();
  const [message, setMessage] = useState();

  useEffect(() => {
    (async () => {
      const response = await fetch('http://localhost:3000/facilities');
      const data = await response.json();
      setFacilities(data.data);
    })();
  }, []);

  const onChange = (data) => {
    setDate(data);
  };

  const handleChooseFacility = (value) => {
    console.log(value);
    setFacility(value);
  };

  const handleGoNext = () => {
    if (!facility) {
      setMessage('Please select facility');
    } else {
      const month = date.getMonth() + 1; // months from 1-12
      const day = date.getDate();
      const year = date.getFullYear();
      chooseDate(`${year}-${month}-${day}`);
      setCurrentStepNext();
      chooseFacility(facility);
    }
  };

  return (
    <Container>
      <Row>
        <Col xs={12} className="mb-3 p-1">
          {message ? <p className="validation-message">{message}</p> : ''}
          <select
            onChange={(event) => handleChooseFacility(event.target.value)}
            name="facilities"
            id="facilities"
          >
            <option>Select the facility</option>
            {facilities ? (
              facilities.map((singleFacility) => {
                return (
                  <option
                    key={singleFacility.facility_id}
                    value={singleFacility.facility_id}
                  >
                    {singleFacility.name}
                  </option>
                );
              })
            ) : (
              <option>is loading</option>
            )}
          </select>
        </Col>
      </Row>
      <Row className="step1-wrap mb-5">
        <Col xs={12} className="d-flex justify-content-center">
          <Calendar onChange={onChange} value={date} />
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Button onClick={handleGoNext} className="form-button form-button-next">
          Next
        </Button>
      </Row>
    </Container>
  );
}
