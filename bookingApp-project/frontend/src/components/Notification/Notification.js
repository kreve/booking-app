import React, { useState, useContext } from 'react';
import './notification.css';
import { AiFillCloseCircle } from 'react-icons/ai';
import { IoMdNotificationsOutline } from 'react-icons/io';

import { AuthApi } from '../../App/AuthApi';

export default function Notification() {
  const [auth] = useContext(AuthApi);
  const [notificationMessage, setNotificationMessage] = useState();
  const socket = io('http://localhost:8080', {
    transports: ['websocket', 'polling'],
  });

  const handleCloseModal = () => {
    setNotificationMessage('');
  };

  socket.on(`priv_notification_${auth.user_id}`, ({ data }) => {
    console.log(data);
    setNotificationMessage(data);
  });

  return (
    <div
      className={`notification-container ${
        notificationMessage ? 'active' : ''
      } `}
    >
      <AiFillCloseCircle
        className="close-btn"
        color="#386641"
        size={25}
        onClick={handleCloseModal}
      />

      <div className="d-flex mt-4">
        <IoMdNotificationsOutline color="#bc4749" size={45} />
        <p className="ml-4">{notificationMessage}</p>
      </div>
    </div>
  );
}
