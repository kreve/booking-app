import React, { useState } from 'react';
import Step1 from '../Step1/Step1';
import Step2 from '../Step2/Step2';
import Step3 from '../Step3/Step3';

export default function CalendarForm() {
  const [currentStep, setCurrentStep] = useState(1);
  const [chosenDate, setChosenDate] = useState('');
  const [chosenFacility, setChosenFacility] = useState('');

  console.log(chosenDate);

  const handleSetChosenDate = (date) => {
    setChosenDate(date);
  };

  const handleSetChosenFacility = (facility) => {
    setChosenFacility(facility);
  };

  const handleGoNext = () => {
    setCurrentStep(currentStep + 1);
  };

  const handleGoBack = () => {
    setCurrentStep(currentStep - 1);
  };

  const handleResetSteps = () => {
    setCurrentStep(1);
  };

  if (currentStep === 1) {
    return (
      <Step1
        chooseDate={handleSetChosenDate}
        chooseFacility={handleSetChosenFacility}
        setCurrentStepNext={handleGoNext}
      />
    );
  }
  if (currentStep === 2) {
    return (
      <Step2
        chosenDate={chosenDate}
        chosenFacility={chosenFacility}
        setCurrentStepNext={handleGoNext}
        setCurrentStepBack={handleGoBack}
      />
    );
  }
  return (
    <>
      <Step3 resetSteps={handleResetSteps} />
    </>
  );
}
