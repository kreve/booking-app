import React from 'react';
import { Link } from 'react-router-dom';

const Nav = ({ auth }) => {
  if (auth === false) {
    return (
      <nav>
        <ul>
          <Link to="/login">Login</Link>
        </ul>
      </nav>
    );
  }
  return (
    <nav>
      <ul>
        <Link to="/HomeAdmin">HomeAdmin</Link>
      </ul>
      <ul>
        <Link to="/BookingsAdmin">BookingsAdmin</Link>
      </ul>
      <ul>
        <Link to="/FacilitiesAdmin">FacilitiesAdmin</Link>
      </ul>
      <ul>
        <Link to="/UsersAdmin">UsersAdmin</Link>
      </ul>
      <ul>
        <Link to="/MessagesAdmin">MessagesAdmin</Link>
      </ul>
      <ul>
        <Link to="/logout">Logout</Link>
      </ul>
    </nav>
  );
};

export default Nav;
