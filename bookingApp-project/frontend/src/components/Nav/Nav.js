import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import './nav.css';
import { IoMdAddCircle } from 'react-icons/io';
import { BsCalendar } from 'react-icons/bs';
import { FiLogOut, FiMessageSquare } from 'react-icons/fi';
import { RiUserSettingsLine } from 'react-icons/ri';
import user from '../../assets/images/user.png';
import { AuthApi } from '../../App/AuthApi';

const Nav = () => {
  const [auth] = useContext(AuthApi);

  if (auth.auth === false) {
    return (
      <nav>
        <p>Login to see the content and use the app</p>
      </nav>
    );
  }

  if (auth.user_role === 2) {
    return (
      <nav>
        <ul>
          <Link to="/admin">Home</Link>
        </ul>
        <ul>
          <Link to="/bookings">Bookings</Link>
        </ul>
        <ul>
          <Link to="/facilities">Facilities</Link>
        </ul>
        <ul>
          <Link to="/list">Users</Link>
        </ul>
        <ul>
          <Link to="/messages">Messages</Link>
        </ul>
        <ul>
          <Link to="/addnewuser">Add New User</Link>
        </ul>
        <ul>
          <Link to="/logout">Logout</Link>
        </ul>
      </nav>
    );
  }
  return (
    <nav>
      <div className="imgWrap">
        <img src={user} alt="user-icon" />
      </div>
      <div className="welcome-text">
        {'Hello, '}
        {auth.user_email}
      </div>
      <ul>
        <li className="d-flex align-items-center mb-3">
          <IoMdAddCircle color="#386641" size={25} />
          <Link className="ml-3" to="/addbooking">
            Add booking
          </Link>
        </li>
        <li className="mb-3">
          <BsCalendar color="#386641" size={25} />
          <Link className="ml-3" to="/mybookings">
            My Bookings
          </Link>
        </li>
        <li className="logout-link mb-3">
          <RiUserSettingsLine color="#386641" size={25} />
          <Link className="ml-3" to="/editprofile">
            Edit profile
          </Link>
        </li>
        <li className="logout-link mb-3">
          <FiMessageSquare color="#386641" size={25} />
          <Link className="ml-3" to="/mymessages">
            Contact admin
          </Link>
        </li>
        <li className="logout-link mb-3">
          <FiLogOut color="#386641" size={25} />
          <Link className="ml-3" to="/logout">
            Logout
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
