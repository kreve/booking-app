import React, { useEffect, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AuthApi } from '../../App/AuthApi';
import './logout.css';

const Logout = () => {
  const [auth, setAuth] = useContext(AuthApi);

  useEffect(() => {
    (async () => {
      const response = await fetch('http://localhost:3000/auth/logout');
      const data = await response.json();
      console.log(data);
      setAuth(data);
    })();
  }, []);

  return (
    <Container className="main-container logout-container">
      <Row>
        <Col>
          <strong>Bye bye</strong>
        </Col>
      </Row>
      <Row>
        <Col>
          <Link to="/login">Login</Link>
        </Col>
      </Row>
    </Container>
  );
};

export default Logout;
