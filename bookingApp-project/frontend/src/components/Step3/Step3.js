import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { BsCalendar } from 'react-icons/bs';
import { IoMdAddCircle } from 'react-icons/io';

import './step3.css';

export default function Step3({ resetSteps }) {
  const handleOnClick = () => {
    resetSteps();
  };

  return (
    <Container className="step3-container p-5 mb-5">
      <Row>
        <Col xs={12}>
          <h2 className="text-center font-weight-bold">
            Thank you for the booking !
          </h2>
          <p className="text-center">
            We have sent you a confirmation on your email
          </p>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col className="d-flex flex-column align-items-center">
          <div className="mb-3">
            <BsCalendar color="#386641" size={25} />
            <Link className="ml-3" to="/mybookings">
              See My Bookings
            </Link>
          </div>
          <div>
            <IoMdAddCircle color="#386641" size={25} />
            <Button
              variant="link"
              onClick={handleOnClick}
              className="d-inline ml-3"
            >
              Add more bookings
            </Button>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
