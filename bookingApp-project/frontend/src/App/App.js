import React, { useContext } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import LoginForm from '../pages/LoginForm/LoginForm';
import Header from '../components/Header/Header';
import AddBooking from '../pages/AddBooking/AddBooking';
import UsersList from '../pages/UsersList/UsersList';
import Bookings from '../pages/Bookings/Bookings';
import Messages from '../pages/Messages/Messages';
import Facilities from '../pages/Facilities/Facilities';
import './App.css';
import SignupForm from '../pages/SignupForm/SignupFrom';
import Logout from '../components/Logout/Logout';
import HomeAdmin from '../pages/HomeAdmin/HomeAdmin';
import MyBookings from '../pages/MyBookings/MyBookings';
import NewPasswordForm from '../pages/NewPasswordForm/NewPasswordForm';
import EditProfile from '../pages/EditProfile/EditProfile';
import MyMessages from '../pages/MyMessages/MyMessages';
import Notification from '../components/Notification/Notification';
import { AuthApi, AuthProvider } from './AuthApi';
import NewMessage from '../pages/NewMessage/NewMessage';

function App() {
  return (
    <AuthProvider>
      <Router>
        <Container fluid>
          <Row>
            <Col xs={2} className="p-1">
              <Header />
            </Col>
            <Col xs={10} className="main-content p-1">
              <Routes />
            </Col>
          </Row>
          <Notification />
        </Container>
      </Router>
    </AuthProvider>
  );
}

const Routes = () => {
  const [auth, setAuth] = useContext(AuthApi);

  return (
    <Switch>
      <ProtectedRoute
        auth={auth}
        exact
        path="/addbooking"
        component={AddBooking}
      />
      <ProtectedRoute auth={auth} exact path="/admin" component={HomeAdmin} />
      <Route path="/list" component={UsersList} />
      <Route path="/bookings" component={Bookings} />
      <Route path="/facilities" component={Facilities} />
      <Route path="/messages" component={Messages} />
      <Route path="/login" component={LoginForm} />
      <Route path="/signup" component={SignupForm} />
      <Route exact path="/new_password" component={NewPasswordForm} />
      <Route path="/logout" component={Logout} />
      <ProtectedRoute auth={auth} path="/mybookings" component={MyBookings} />
      <ProtectedRoute auth={auth} path="/editprofile" component={EditProfile} />
      <ProtectedRoute auth={auth} path="/mymessages" component={MyMessages} />
      <ProtectedRoute auth={auth} path="/newmessage" component={NewMessage} />
      <Route
        path="/server-error"
        render={() => (
          <p>
            Oh no! Something bad happened. Please come back later when we fixed
            that problem. Thanks.
          </p>
        )}
      />
      <Route render={() => <h3>Page not found</h3>} />
    </Switch>
  );
};

const ProtectedRoute = ({ auth, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => (auth ? <Component /> : <Redirect to="/login" />)}
    />
  );
};

export default App;
