import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';
// import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';

export default function Bookings() {
  const [isLoading, setIsLoading] = useState(true);
  const [bookings, setBookings] = useState([]);

  useEffect(() => {
    setIsLoading(false);
    loadData();
  }, []);

  const loadData = async () => {
    const api = 'http://localhost:8080/bookings';
    const response = await fetch(api);
    const data = await response.json();
    console.log(data);
    setBookings(data);
  };

  return (
    <div className="main-container">
      <div className="row">
        <div className="col-sm-2" />
        <div className="col-sm-8">
          <h1>Bookings</h1>
          {isLoading ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Room</th>
                  <th>User email</th>
                </tr>
              </thead>
              <tbody>
                {bookings.map((booking) => {
                  return (
                    <tr key={booking.booking_id}>
                      <td>{booking.date}</td>
                      <td>{booking.time}</td>
                      <td>
                        Facility id is:
                        {booking.facility_id}
                      </td>
                      <td>{booking.user_email}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          )}
        </div>
        <div className="col-sm-2" />
      </div>
    </div>
  );
}
