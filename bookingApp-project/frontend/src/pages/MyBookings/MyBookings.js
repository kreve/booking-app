import React, { useState, useEffect, useContext } from 'react';
import Loader from 'react-loader-spinner';
import { Container, Row, Col } from 'react-bootstrap';
import { MdDeleteForever } from 'react-icons/md';
import { AuthApi } from '../../App/AuthApi';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import './mybookings.css';

export default function MyBookings() {
  const [auth] = useContext(AuthApi);
  const [myCurrentBookings, setMyCurrentBookings] = useState(false);
  const [myArchiveBookings, setMyArchiveBookings] = useState(false);

  useEffect(() => {
    fetchCurrentBookings('from_date');
    fetchCurrentBookings('to_date');
  }, []);

  const deleteBooking = (bookingId) => {
    (async () => {
      const response = await fetch(
        `http://localhost:3000/bookings/${bookingId}`,
        {
          method: 'DELETE',
        }
      );
      const data = await response.json();
      fetchCurrentBookings('from_date');
    })();
  };

  async function fetchCurrentBookings(fromOrTodDate) {
    const today = new Date().toISOString().split('T')[0];
    const response = await fetch(
      `http://localhost:3000/bookings/filter?${fromOrTodDate}=${today}&user_id=${auth.user_id}`
    );
    const data = await response.json();
    // console.log(data);
    if (fromOrTodDate === 'from_date') {
      setMyCurrentBookings(data.data);
    } else {
      setMyArchiveBookings(data.data);
    }
  }

  return (
    <Container className="main-container justify-content-start my-bookings-content">
      <h2>Current bookings</h2>
      <Row className="justify-content-center mb-5">
        <Col md={12}>
          {!myCurrentBookings ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <table className="my-bookings-table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Room</th>
                  <th>Cancle</th>
                </tr>
              </thead>
              <tbody>
                {myCurrentBookings.map((booking) => {
                  return (
                    <tr key={booking.booking_id}>
                      <td>
                        {new Date(booking.date).toLocaleDateString('en-GB')}
                      </td>
                      <td>{`${booking.time}.00 - ${booking.time + 1}.00`}</td>
                      <td>{booking.name}</td>
                      <td>
                        <MdDeleteForever
                          onClick={() => deleteBooking(booking.booking_id)}
                          size={30}
                          color="#BC4749"
                        />
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          )}
        </Col>
      </Row>
      <h4>Archive</h4>
      <Row className="justify-content-center">
        <Col md={12}>
          {!myArchiveBookings ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <table className="my-bookings-table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Room</th>
                </tr>
              </thead>
              <tbody>
                {myArchiveBookings.map((booking) => {
                  return (
                    <tr key={booking.booking_id}>
                      <td>
                        {new Date(booking.date).toLocaleDateString('en-GB')}
                      </td>
                      <td>{`${booking.time}.00 - ${booking.time + 1}.00`}</td>
                      <td>{booking.name}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          )}
        </Col>
      </Row>
    </Container>
  );
}
