import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import { AuthApi } from '../../App/AuthApi';
import './loginForm.css';

function LoginForm(props) {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [message, setMessage] = useState();
  const [auth, setAuth] = useContext(AuthApi);

  const history = useHistory();

  const handleInputEmailChanged = (event) => {
    setEmail(event.target.value);
  };

  const handleInputPasswordChanged = (event) => {
    setPassword(event.target.value);
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    const requestOptions = {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };

    // fetchUserData:
    (async () => {
      try {
        const response = await fetch(
          'http://localhost:3000/auth/login',
          requestOptions
        );
        if (!response.ok) {
          if (response.status === 403 || response.status === 429) {
            const data = await response.json();
            setMessage(data.message);
          } else {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
        } else {
          const data = await response.json();
          console.log(data.user_role);
          setAuth(data);
          if (data.user_role === 1) {
            history.push('/mybookings');
          } else {
            history.push('/admin');
          }
        }
      } catch (error) {
        console.error(error);
        setMessage('Ups! Something went wrong. Please try again');
      }
    })();
  };

  return (
    <div className="login-form-wrap">
      <h2>Login</h2>
      <Form onSubmit={handleFormSubmit}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            onChange={handleInputEmailChanged}
            required
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            onChange={handleInputPasswordChanged}
            required
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Login
        </Button>
      </Form>
      {message ? <p>{message}</p> : ''}
      <a href="/signup">sign up</a>
    </div>
  );
}

export default LoginForm;
