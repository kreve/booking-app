const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth.js');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(authRoutes.requireAuth);

// get all messages

router.get('/messages', (req, res) => {
  const str = `SELECT * FROM messages`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send({ data: 'ok' });
  });
});

// get all messages filtred

router.get('/messages/filter', (req, res) => {
  if (req.query.recipient_id) {
    const sqlSelect = `SELECT * FROM messages LEFT JOIN users ON messages.sender_id=users.user_id WHERE recipient_id = ?`;

    connection.query(sqlSelect, [req.query.recipient_id], (error, results) => {
      if (error) throw error;
      const messages = results;
      return res.send({ data: messages });
    });
  } else if (req.query.sender_id) {
    const sqlSelect = `SELECT * FROM messages LEFT JOIN users ON messages.sender_id=users.user_id WHERE sender_id = ?`;

    connection.query(sqlSelect, [req.query.sender_id], (error, results) => {
      if (error) throw error;
      const messages = results;
      return res.send({ data: messages });
    });
  }
});

// create a new messgae

router.post('/messages', (req, res) => {
  const { senderId } = req.body;
  const { recipientId } = req.body;
  const { subject } = req.body;
  const { message } = req.body;
  const { date } = req.body;

  const sqlInsert =
    'INSERT INTO messages (sender_id, recipient_id, subject, message, date) VALUES ( ?,?,?,?,?);';

  connection.query(
    sqlInsert,
    [senderId, recipientId, subject, message, date],
    (error, results) => {
      if (error) throw error;
      return res.send({ message: 'Your message has been sent successfully' });
    }
  );
});

module.exports = router;
