const router = require('express').Router();
const bodyParser = require('body-parser');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

// get all facilities

router.get('/facilities', (req, res) => {
  const sqlSelect = `SELECT * FROM facilities`;

  connection.query(sqlSelect, (error, results) => {
    if (error) throw error;
    const facilities = results;
    console.log(facilities);
    return res.send({ data: facilities });
  });
});

module.exports = router;
